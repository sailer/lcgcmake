#!/bin/bash

#---Look for the location of the sftnight.keytab file
if [ -e /ec/conf/sftnight.keytab ]; then
  kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
elif [ -e $WORKSPACE/../../conf/sftnight.keytab ]; then
  kinit sftnight@CERN.CH -5 -V -k -t $WORKSPACE/../../conf/sftnight.keytab
elif [ `uname -s` == Darwin ]; then
  kinit -V -k -t /build/conf/sftnight.keytab
fi

#---Check status code
if [ $? -ne 0 ]; then
  echo "Could not get (kinit) credentials! Aborting."
  exit 1
else
  klist
fi
