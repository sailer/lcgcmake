From d97dae7f5de3402f2555c4316717fbcd7531c5a7 Mon Sep 17 00:00:00 2001
From: Frank Siegert <frank.siegert@cern.ch>
Date: Tue, 2 Jun 2020 23:00:04 +0200
Subject: [PATCH] Bugfix for crash with `SPECIAL_TAU_SPIN_CORRELATIONS`

Events with partonic hadron decays involving a tau (e.g. B -> cb d tau vtau) and multiple parton interactions could trigger a core dump in the `SPECIAL_TAU_SPIN_CORRELATIONS=1` treatment. This happened due to an infinite loop, which is fixed now.
In addition, in events triggering a `Retry_Event` a minor problem with lost spin correlations is fixed.
---
 METOOLS/SpinCorrelations/Amplitude2_Matrix.C |  2 +-
 METOOLS/SpinCorrelations/Spin_Density.C      |  5 +++--
 SHERPA/Single_Events/Decay_Handler_Base.C    | 15 ++++++++-------
 3 files changed, 12 insertions(+), 10 deletions(-)

diff --git a/METOOLS/SpinCorrelations/Amplitude2_Matrix.C b/METOOLS/SpinCorrelations/Amplitude2_Matrix.C
index b8071acc7..2d1d3dc70 100644
--- METOOLS/SpinCorrelations/Amplitude2_Matrix.C
+++ METOOLS/SpinCorrelations/Amplitude2_Matrix.C
@@ -55,7 +55,7 @@ Complex Amplitude2_Matrix::operator*(const Amplitude2_Matrix& sigma) const
 namespace METOOLS {
   std::ostream& operator<<(std::ostream& ostr, const Amplitude2_Matrix& m) {
     ostr<<"   Matrix with "<<m.m_nhel<<" spin combinations for "
-        <<m.Particle()->RefFlav()<<":"<<std::endl;
+        <<(m.Particle()?m.Particle()->RefFlav():Flavour(kf_none))<<":"<<std::endl;
     for(size_t i=0;i<m.m_nhel;i++) {
       for(size_t j=0;j<m.m_nhel;j++) {
         ostr<<m(i,j)<<", ";
diff --git a/METOOLS/SpinCorrelations/Spin_Density.C b/METOOLS/SpinCorrelations/Spin_Density.C
index dbb7f4f7b..ae1de5dba 100644
--- METOOLS/SpinCorrelations/Spin_Density.C
+++ METOOLS/SpinCorrelations/Spin_Density.C
@@ -42,7 +42,7 @@ Spin_Density::~Spin_Density()
 {
 }
 
-typedef std::map<Particle*, Spin_Density*> SpinDensityMap;
+typedef std::vector<std::pair<std::pair<ATOOLS::Flavour,ATOOLS::Vec4D>, Spin_Density*> > SpinDensityMap;
 namespace ATOOLS {
   template <> Blob_Data<SpinDensityMap*>::~Blob_Data()
   {
@@ -56,7 +56,8 @@ namespace ATOOLS {
   {
     SpinDensityMap* newdata = new SpinDensityMap();
     for (SpinDensityMap::iterator it = m_data->begin(); it!=m_data->end(); ++it) {
-      newdata->insert(make_pair(it->first, new Spin_Density(*it->second)));
+      std::pair<ATOOLS::Flavour,ATOOLS::Vec4D> first = std::make_pair(it->first.first, it->first.second);
+      newdata->push_back(make_pair(first, new Spin_Density(*it->second)));
     }
     return new Blob_Data(newdata);
   }
diff --git a/SHERPA/Single_Events/Decay_Handler_Base.C b/SHERPA/Single_Events/Decay_Handler_Base.C
index 8106a68ef..f1aa89104 100644
--- SHERPA/Single_Events/Decay_Handler_Base.C
+++ SHERPA/Single_Events/Decay_Handler_Base.C
@@ -183,7 +183,7 @@ Blob* FindSPBlob(Blob* startblob)
 }
 
 
-typedef map<Particle*, Spin_Density*> SpinDensityMap;
+typedef std::vector<std::pair<std::pair<ATOOLS::Flavour,ATOOLS::Vec4D>, Spin_Density*> > SpinDensityMap;
 bool Decay_Handler_Base::DoSpecialDecayTauSC(Particle* part)
 {
   DEBUG_FUNC(*part);
@@ -207,9 +207,8 @@ bool Decay_Handler_Base::DoSpecialDecayTauSC(Particle* part)
 
   double bestDeltaR=1000.0; Spin_Density* sigma_tau=NULL;
   for (SpinDensityMap::iterator it=tau_spindensity->begin(); it!=tau_spindensity->end(); ++it) {
-    Particle* testpart = it->first;
-    if (testpart->Flav()==part->Flav()) {
-      double newDeltaR=part->Momentum().DR(testpart->Momentum());
+    if (it->first.first==part->Flav()) {
+      double newDeltaR=part->Momentum().DR(it->first.second);
       if (newDeltaR<bestDeltaR) {
         bestDeltaR=newDeltaR;
         sigma_tau=it->second;
@@ -378,7 +377,7 @@ Decay_Matrix* Decay_Handler_Base::FillDecayTree(Blob * blob, Spin_Density* s0)
          blob->Has(blob_status::needs_showers))) {
       DEBUG_INFO("is stable.");
       if (m_specialtauspincorr && daughters[i]->Flav().Kfcode()==kf_tau &&
-          !daughters[i]->Flav().IsStable() &&
+          !daughters[i]->Flav().IsStable() && blob->Type()==btp::Hard_Decay &&
           rpa->gen.SoftSC()) {
         DEBUG_INFO("  keeping tau spin information for hadronic tau decays.");
         SpinDensityMap* tau_spindensity;
@@ -392,8 +391,10 @@ Decay_Matrix* Decay_Handler_Base::FillDecayTree(Blob * blob, Spin_Density* s0)
         else {
           tau_spindensity = bdb->Get<SpinDensityMap*>();
         }
-        (*tau_spindensity)[daughters[i]] = new Spin_Density(daughters[i],amps);
-        DEBUG_VAR(*(*tau_spindensity)[daughters[i]]);
+        tau_spindensity->push_back(make_pair(make_pair(daughters[i]->Flav(), daughters[i]->Momentum()),
+                                             new Spin_Density(daughters[i],amps)));
+        DEBUG_VAR(*(tau_spindensity->back().second));
+        tau_spindensity->back().second->SetParticle(NULL);
       }
       else if (m_spincorr) {
         Decay_Matrix* D=new Decay_Matrix(daughters[i]);
