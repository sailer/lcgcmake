#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT    v6-22-00-patches  GIT=http://root.cern.ch/git/root.git )
#LCG_external_package(ROOT    v6.22.02 )

# Remove COOL/CORAL for time being (tests, at least, failing)
if(${LCG_COMP} MATCHES clang10)
  LCG_remove_package(COOL)
  LCG_remove_package(CORAL)
endif()
LCG_external_package(madgraph5amc      2.8.1.atlas     ${MCGENPATH}/madgraph5amc author=2.8.1)
