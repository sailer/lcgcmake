#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

LCG_external_package(spark           3.0.1-cern1                              )
LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )
