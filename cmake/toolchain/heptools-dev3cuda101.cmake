#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 2)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

# Downgrade the C++ standard since Cuda (9) does not support c++17
#    This is a bit of hack since packages already build with a given hash value might have been built 
#    a different standard. It is OK for the nightlies for the time being.
if(LCG_CPP17)
  set(LCG_CPP17 FALSE)
  set(LCG_CPP14 TRUE)
endif()

#---Overwrites and additional packages ----------------------------
LCG_external_package(ROOT         HEAD   GIT=http://root.cern.ch/git/root.git        )
LCG_external_package(hepmc3       HEAD   GIT=https://gitlab.cern.ch/hepmc/HepMC3.git )
LCG_external_package(tensorflow   1.14.0                        )
LCG_external_package(tensorflow_estimator  1.14.0               )
LCG_external_package(tensorboard  1.14.0    protobuf=3.6.1      )
LCG_external_package(cuda         10.1      full=10.1.168_418.67)
LCG_external_package(cudnn        7.6.1.34                      )
