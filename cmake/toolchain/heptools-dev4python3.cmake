#---List of externals----------------------------------------------
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

LCG_external_package(ROOT    v6-22-00-patches   GIT=http://root.cern.ch/git/root.git )

# if(NOT ${LCG_OS}${LCG_OSVERS} MATCHES fc|ubuntu|mac[0-9]+)
#  LCG_AA_project(CORAL    master     GIT=https://gitlab.cern.ch/lcgcoral/coral.git )
#  LCG_AA_project(COOL     master     GIT=https://gitlab.cern.ch/lcgcool/cool.git   )
# endif()

if(APPLE)    # We cannot build ROOT (HEAD) and R together because RConfig.h file clashes
  LCG_remove_package(R)
  LCG_remove_package(rpy2)
endif()
