#!/usr/bin/env python
"""
Program to install locally the LCG binary tarfile. It downsloads, expands and relocates the binary tarfile. 
<pere.mato@cern.ch>
Version 1.0
"""

#-------------------------------------------------------------------------------
from __future__ import print_function
import os, sys, tarfile, subprocess, shutil
try:
    import argparse
except ImportError:
    import argparse2 as argparse

if sys.version_info[0] >= 3 :
  import urllib.request as urllib2
else:
  import urllib2

#---install_tarfile-------------------------------------------------------------
def install_tarfile(urltarfile, prefix, lcgprefix):
  try :
    filename = os.path.basename(urltarfile)
    items = os.path.splitext(filename)[0].split('-')
    hash = items[-5].split('_')[-1]
    platform = '-'.join(items[-4:])
  except:
    print("Binary tarfile name '%s' ill-formed" % filename)
    sys.exit(1)

  print('==== Downloading and installing %s' % urltarfile)
  try:
    resp = urllib2.urlopen(urltarfile)
    tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
    dirname, version = os.path.split(tar.next().name)
    tar.extractall(path=prefix)
  except urllib2.HTTPError as detail:
    print('Error downloading %s : %s' % (urltarfile, detail))
    sys.exit(1)
  except tarfile.ReadError as detail:
    print('Error untaring %s : %s' %(urltarfile, detail))
    sys.exit(1)
  except:
    print('Unexpected error:', sys.exc_info()[0])
    sys.exit(1)

  #---rename the version directory
  old_dirname = os.path.join(prefix, dirname, version)
  new_dirname = os.path.join(prefix, dirname, version + '-' + hash)
  if not os.path.exists(new_dirname): os.mkdir(new_dirname)
  if os.path.exists(os.path.join(new_dirname, platform)): shutil.rmtree(os.path.join(new_dirname, platform))
  os.rename(os.path.join(old_dirname, platform), os.path.join(new_dirname, platform))
  shutil.rmtree(old_dirname)

  #---run the post-install
  postinstall = os.path.join(prefix, dirname, version + '-' + hash, platform, '.post-install.sh')
  if os.path.exists(postinstall) :
    os.environ['INSTALLDIR'] = prefix
    if lcgprefix  :
      os.environ['LCGRELEASES'] = lcgprefix.replace(';',':').replace(' ',':')
    with open(os.devnull, 'w') as devnull:
      rc = subprocess.call(['/bin/bash',postinstall], stdout=devnull)
      if rc != 0:
        raise RuntimeError("Post-install for package {0} failed!".format(filename))

#---Main program----------------------------------------------------------------
if __name__ == '__main__':

  #---Parse the arguments-------------------------------------------------------
  parser = argparse.ArgumentParser()
  parser.add_argument('--url', dest='url', help='URL of the binary tarfile', required=True)
  parser.add_argument('--prefix', dest='prefix', help='prefix to the installation', required=True)
  parser.add_argument('--lcgprefix', dest='lcgprefix', help='LCG prefix to the installation', default='', required=False)
  args = parser.parse_args()

  install_tarfile(args.url, args.prefix, args.lcgprefix)
